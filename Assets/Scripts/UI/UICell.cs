﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICell : MonoBehaviour, IListCell {

	public Text text;


	private string number;


	public void Init(object data) {
		number = (string)data;
		text.text = number.ToString ();
	}

	//no need to change this every frame
//	void Update() {
//		text.text = number.ToString();
//	}



}
